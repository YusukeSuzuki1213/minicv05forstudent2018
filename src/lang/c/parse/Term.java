package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Term extends CParseRule {
	// term ::= factor {termMult | termDiv}
	private CParseRule term;
	public Term(CParseContext pcx) {
	}
	public static boolean isFirst(CToken tk) {
		return Factor.isFirst(tk);
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		// ここにやってくるときは、必ずisFirst()が満たされている
		CParseRule factor = new Factor(pcx);
		factor.parse(pcx);

		CTokenizer ct = pcx.getTokenizer();
		CToken tk = ct.getCurrentToken(pcx);
		CParseRule list = null;
		while (TermMult.isFirst(tk) || TermDiv.isFirst(tk)) {
			if(TermMult.isFirst(tk)){
				list = new TermMult(pcx, factor);
			} else if (TermDiv.isFirst(tk)){
				list = new TermDiv(pcx, factor);
			} else {
				pcx.fatalError("Termクラスでエラー");
			}
			list.parse(pcx);
			factor = list;
			tk = ct.getCurrentToken(pcx);
		}
		term = factor;
	}
	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		if (term != null) {
			term.semanticCheck(pcx);
			this.setCType(term.getCType());		// term の型をそのままコピー
			this.setConstant(term.isConstant());
		}
	}

	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		o.println(";;; term starts");
		if (term != null) { term.codeGen(pcx); }
		o.println(";;; term completes");
	}
}

class TermMult extends CParseRule {
	private CToken op;
	private CParseRule right, left;
	public TermMult(CParseContext pcxm, CParseRule left) {
		this.left = left;
	}
	public static boolean isFirst(CToken tk) {
		return tk.getType() == CToken.TK_MULT;
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		CTokenizer ct = pcx.getTokenizer();
		op = ct.getCurrentToken(pcx);
		// *の次の字句を読む
		CToken tk = ct.getNextToken(pcx);
		if (Factor.isFirst(tk)) {
			right = new Factor(pcx);
			right.parse(pcx);
		} else {
			pcx.fatalError(tk.toExplainString() + "*の後ろはfactorです");
		}

	}
	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		// 乗算の型計算規則

		final int s[][] = {
				//		T_err			T_int			T_pint			T_aint			T_paint
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},	// T_err
				{	CType.T_err,	CType.T_int,	CType.T_err,	CType.T_err,	CType.T_err},	// T_int
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},  // T_pint
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},	// T_aint
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},	// T_paint
		};
		if (left != null && right != null) {
			left.semanticCheck(pcx);
			right.semanticCheck(pcx);
			int lt = left.getCType().getType();		//*の左辺の型
			int rt = right.getCType().getType();	//*の右辺の型
			int nt = s[lt][rt];						// 規則による型計算
			if (nt == CType.T_err) {
				pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]はかけれません");
			}
			this.setCType(CType.getCType(nt));
			this.setConstant(left.isConstant() && right.isConstant());	// *の左右両方が定数のときだけ定数
		}
	}
	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		if (left != null && right != null) {
			left.codeGen(pcx);		// 左部分木のコード生成を頼む
			//o.println("\tMOV\tR0, (R6)+   \t; TermMult:左辺をスタックに積む");
			right.codeGen(pcx);		// 右部分木のコード生成を頼む
			//o.println("\tMOV\tR0, (R6)+   \t; TermMult:右辺をスタックに積む");
			o.println("\tJSR\tMUL       \t; TermMult: 掛け算のサブルーチン呼び出し(結果はR0に？)");
			o.println("\tSUB\t#2, R6    \t; TermMult: 引数を格納していた分スタックを戻す");
			o.println("\tMOV\tR0, (R6)+\t; TermMult: 掛け算の結果を積む");
		}
	}


}

class TermDiv extends CParseRule {
	private CToken op;
	private CParseRule right,left;
	public TermDiv(CParseContext pcx, CParseRule left) {
		this.left = left;
	}
	public static boolean isFirst(CToken tk) {
		return tk.getType() == CToken.TK_DIV;
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		CTokenizer ct = pcx.getTokenizer();
		op = ct.getCurrentToken(pcx);
		// /の次の字句を読む
		CToken tk = ct.getNextToken(pcx);
		if (Factor.isFirst(tk)) {
			right = new Factor(pcx);
			right.parse(pcx);
		} else {
			pcx.fatalError(tk.toExplainString() + "/の後ろはfactorです");
		}


	}
	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		// 徐算の型計算規則
		final int s[][] = {
				//		T_err			T_int			T_pint			T_inta			T_pinta
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},	// T_err
				{	CType.T_err,	CType.T_int,	CType.T_err,	CType.T_err,	CType.T_err},	// T_int
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},  // T_pint
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},	// T_inta
				{	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err,	CType.T_err},	// T_pinta
		};
		if (left != null && right != null) {
			left.semanticCheck(pcx);
			right.semanticCheck(pcx);
			int lt = left.getCType().getType();		//*の左辺の型
			int rt = right.getCType().getType();	//*の右辺の型
			int nt = s[lt][rt];						// 規則による型計算
			if (nt == CType.T_err) {
				pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType().toString() + "]と右辺の型[" + right.getCType().toString() + "]はわれません");
			}
			this.setCType(CType.getCType(nt));
			this.setConstant(left.isConstant() && right.isConstant());	// *の左右両方が定数のときだけ定数
		}
	}
	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		if (left != null && right != null) {
			left.codeGen(pcx);		// 左部分木のコード生成を頼む
			//o.println("\tMOV\tR0, (R6)+   \t; Termdiv:左辺をスタックに積む");
			right.codeGen(pcx);		// 右部分木のコード生成を頼む
			//o.println("\tMOV\tR0, (R6)+   \t; TermDivt:右辺をスタックに積む");
			o.println("\tJSR\tDIV       \t; TermDiv: 割り算のサブルーチン呼び出し(結果はR0に？)");
			o.println("\tSUB\t#2, R6    \t; TermDiv: 引数を格納していた分スタックを戻す");
			o.println("\tMOV\tR0, (R6)+\t; TermDiv: 掛け算の結果を積む");
		}
	}
}