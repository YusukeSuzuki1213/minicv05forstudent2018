package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;


public class Statement extends CParseRule {
    private CToken tk;
    private CParseRule statement;

    public Statement(CParseContext pcx){

    }
    public static boolean isFirst(CToken tk) {
        return StatementAssign.isFirst(tk);
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);
        statement = new StatementAssign(pcx);
        statement.parse(pcx);
    }
    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (statement != null) {
            statement.semanticCheck(pcx);
            this.setCType(statement.getCType());		// statement の型をそのままコピー
            this.setConstant(statement.isConstant());
        }
    }
    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; Statement starts");
        if( statement!= null){
            statement.codeGen(pcx);
        }
        o.println(";;; Statement completes");
    }
}
