package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Primary extends CParseRule {
    private CParseRule primary;
    public Primary(CParseContext pcx) {
    }
    public CParseRule getChild() {
        return primary;
    }
    public static boolean isFirst(CToken tk) {
        return PrimaryMult.isFirst(tk) || Variable.isFirst(tk);
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);

        if (PrimaryMult.isFirst(tk)) {
            primary = new PrimaryMult(pcx);
        } else if (Variable.isFirst(tk)){
            primary = new Variable(pcx);
        } else {
            pcx.fatalError("primaryクラスでエラー");
        }
        primary.parse(pcx);
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (primary != null) {
            primary.semanticCheck(pcx);
            this.setCType(primary.getCType());		// primary の型をそのままコピー
            this.setConstant(primary.isConstant());
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; Primary starts");
        if(primary != null){
            primary.codeGen(pcx);
        }
        o.println(";;; Primary completes");
    }
}
